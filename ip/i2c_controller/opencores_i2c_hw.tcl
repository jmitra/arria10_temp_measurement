# TCL File Generated by Component Editor 14.1
# Tue Apr 07 13:51:25 CEST 2015
# DO NOT MODIFY


# 
# opencores_i2c "OpenCores I2C" v1.0
#  2015.04.07.13:51:25
# 
# 

# 
# request TCL package from ACDS 14.1
# 
package require -exact qsys 14.1


# 
# module opencores_i2c
# 
set_module_property DESCRIPTION ""
set_module_property NAME opencores_i2c
set_module_property VERSION 1.0
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP Custom
set_module_property AUTHOR ""
set_module_property DISPLAY_NAME "OpenCores I2C"
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false


# 
# file sets
# 
add_fileset quartus_synth QUARTUS_SYNTH "" "Quartus Synthesis"
set_fileset_property quartus_synth TOP_LEVEL opencores_i2c
set_fileset_property quartus_synth ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property quartus_synth ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file i2c_master_bit_ctrl.v VERILOG PATH i2c_master_bit_ctrl.v
add_fileset_file i2c_master_byte_ctrl.v VERILOG PATH i2c_master_byte_ctrl.v
add_fileset_file i2c_master_defines.v VERILOG PATH i2c_master_defines.v
add_fileset_file i2c_master_top.v VERILOG PATH i2c_master_top.v
add_fileset_file opencores_i2c.v VERILOG PATH opencores_i2c.v TOP_LEVEL_FILE
add_fileset_file timescale.v VERILOG PATH timescale.v

add_fileset sim_verilog SIM_VERILOG "" "Verilog Simulation"
set_fileset_property sim_verilog TOP_LEVEL opencores_i2c
set_fileset_property sim_verilog ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property sim_verilog ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file i2c_master_bit_ctrl.v VERILOG PATH i2c_master_bit_ctrl.v
add_fileset_file i2c_master_byte_ctrl.v VERILOG PATH i2c_master_byte_ctrl.v
add_fileset_file i2c_master_defines.v VERILOG PATH i2c_master_defines.v
add_fileset_file i2c_master_top.v VERILOG PATH i2c_master_top.v
add_fileset_file opencores_i2c.v VERILOG PATH opencores_i2c.v
add_fileset_file timescale.v VERILOG PATH timescale.v


# 
# parameters
# 


# 
# display items
# 


# 
# connection point clk
# 
add_interface clk clock end
set_interface_property clk clockRate 0
set_interface_property clk ENABLED true
set_interface_property clk EXPORT_OF ""
set_interface_property clk PORT_NAME_MAP ""
set_interface_property clk CMSIS_SVD_VARIABLES ""
set_interface_property clk SVD_ADDRESS_GROUP ""

add_interface_port clk wb_clk_i clk Input 1


# 
# connection point clk_reset
# 
add_interface clk_reset reset end
set_interface_property clk_reset associatedClock clk
set_interface_property clk_reset synchronousEdges DEASSERT
set_interface_property clk_reset ENABLED true
set_interface_property clk_reset EXPORT_OF ""
set_interface_property clk_reset PORT_NAME_MAP ""
set_interface_property clk_reset CMSIS_SVD_VARIABLES ""
set_interface_property clk_reset SVD_ADDRESS_GROUP ""

add_interface_port clk_reset wb_rst_i reset Input 1


# 
# connection point avalon_i2c_slave
# 
add_interface avalon_i2c_slave avalon end
set_interface_property avalon_i2c_slave addressAlignment NATIVE
set_interface_property avalon_i2c_slave addressUnits WORDS
set_interface_property avalon_i2c_slave associatedClock clk
set_interface_property avalon_i2c_slave associatedReset clk_reset
set_interface_property avalon_i2c_slave bitsPerSymbol 8
set_interface_property avalon_i2c_slave burstOnBurstBoundariesOnly false
set_interface_property avalon_i2c_slave burstcountUnits WORDS
set_interface_property avalon_i2c_slave explicitAddressSpan 0
set_interface_property avalon_i2c_slave holdTime 0
set_interface_property avalon_i2c_slave linewrapBursts false
set_interface_property avalon_i2c_slave maximumPendingReadTransactions 0
set_interface_property avalon_i2c_slave maximumPendingWriteTransactions 0
set_interface_property avalon_i2c_slave readLatency 0
set_interface_property avalon_i2c_slave readWaitTime 1
set_interface_property avalon_i2c_slave setupTime 0
set_interface_property avalon_i2c_slave timingUnits Cycles
set_interface_property avalon_i2c_slave writeWaitTime 0
set_interface_property avalon_i2c_slave ENABLED true
set_interface_property avalon_i2c_slave EXPORT_OF ""
set_interface_property avalon_i2c_slave PORT_NAME_MAP ""
set_interface_property avalon_i2c_slave CMSIS_SVD_VARIABLES ""
set_interface_property avalon_i2c_slave SVD_ADDRESS_GROUP ""

add_interface_port avalon_i2c_slave wb_adr_i address Input 3
add_interface_port avalon_i2c_slave wb_dat_i writedata Input 8
add_interface_port avalon_i2c_slave wb_dat_o readdata Output 8
add_interface_port avalon_i2c_slave wb_we_i write Input 1
add_interface_port avalon_i2c_slave wb_stb_i chipselect Input 1
add_interface_port avalon_i2c_slave wb_ack_o waitrequest_n Output 1
set_interface_assignment avalon_i2c_slave embeddedsw.configuration.isFlash 0
set_interface_assignment avalon_i2c_slave embeddedsw.configuration.isMemoryDevice 0
set_interface_assignment avalon_i2c_slave embeddedsw.configuration.isNonVolatileStorage 0
set_interface_assignment avalon_i2c_slave embeddedsw.configuration.isPrintableDevice 0


# 
# connection point irq
# 
add_interface irq interrupt end
set_interface_property irq associatedAddressablePoint avalon_i2c_slave
set_interface_property irq associatedClock clk
set_interface_property irq associatedReset clk_reset
set_interface_property irq bridgedReceiverOffset ""
set_interface_property irq bridgesToReceiver ""
set_interface_property irq ENABLED true
set_interface_property irq EXPORT_OF ""
set_interface_property irq PORT_NAME_MAP ""
set_interface_property irq CMSIS_SVD_VARIABLES ""
set_interface_property irq SVD_ADDRESS_GROUP ""

add_interface_port irq wb_inta_o irq Output 1


# 
# connection point scl_sda
# 
add_interface scl_sda conduit end
set_interface_property scl_sda associatedClock ""
set_interface_property scl_sda associatedReset ""
set_interface_property scl_sda ENABLED true
set_interface_property scl_sda EXPORT_OF ""
set_interface_property scl_sda PORT_NAME_MAP ""
set_interface_property scl_sda CMSIS_SVD_VARIABLES ""
set_interface_property scl_sda SVD_ADDRESS_GROUP ""

add_interface_port scl_sda scl_pad_io scl Bidir 1
add_interface_port scl_sda sda_pad_io sda Bidir 1

