-----------------------------------------------------------------------------------------------------
-- Title      : Temperature Monitor
-- Project    : Monitor Arria 10 FPGA temperature using internal Temperature Sensing Diode (TSD)
-----------------------------------------------------------------------------------------------------
-- File       : temp_conduit_to_avalon_mm.vhd
-- Author     : Jubin MITRA
-- Contact	  : jubin.mitra@cern.ch
-- Company    : VECC, Kolkata, India
-- Created    : 22-02-2016
-- Last update: 22-02-2016
-- Platform   : 
-- Standard   : VHDL'93/08
-----------------------------------------------------------------------------------------------------
-- Description: 
-- This file converts the temperature sence conduit signals to Arria10 Avalon MM signals	
-- Formula to calculate: (REf: https://documentation.altera.com/#/00045071-AA$AA00044865)
-- Temperature = {(AxC)/1024} - B

--Where:

--    A = 693
--    B = 265
--    C = decimal value of tempout[9..0]

-- Degree Fahrenheit = [180*(Degree celcius)/100.00] + 32
-----------------------------------------------------------------------------------------------------
-- Reference :
-- https://www.altera.com/en_US/pdfs/literature/ug/ug_alttemp_sense.pdf					
-----------------------------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 22-02-2016  1.0      Jubin	Created
-----------------------------------------------------------------------------------------------------



--=================================================================================================--
--#################################################################################################--
--=================================================================================================--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

--=================================================================================================--
--#######################################   Entity   ##############################################--
--=================================================================================================--

entity temp_conduit_to_avalon_mm is 
    port(
    RESET_I                                           : in  std_logic;
    RESET_O                                           : out  std_logic;
    CORECTL_O                                      	  : out std_logic;
    EOC_I                                             : in std_logic;
    TEMPOUT_I                                         : in std_logic_vector( 9 downto 0 );
     
	  
    --======================--
    --   AVALON MM Slave    --
    --======================-- 
	csi_monitor_clk                                                : in std_logic;
	avs_monitor_read                                               : in std_logic;
	avs_monitor_readdata                                           : out std_logic_vector(31 downto 0);
	avs_monitor_write                                              : in std_logic;
	avs_monitor_writedata                                          : in std_logic_vector(31 downto 0);
	avs_monitor_address                                            : in std_logic_vector( 0 downto 0)
    
);
end entity temp_conduit_to_avalon_mm;


--=================================================================================================--
--####################################   Architecture   ###########################################-- 
--=================================================================================================--

architecture mixed of temp_conduit_to_avalon_mm is

	--==================================== Constant Definition ====================================--   
	

	--==================================== Signal Definition ======================================--   

    signal soft_reset                                  : std_logic := '0';
    signal user_reset                                  : std_logic := '0';
    signal reset                                       : std_logic := '0';
    signal eoc_d31                                     : std_logic_vector ( 31 downto 0) := ( others => '0' );
    signal temp_value                                  : std_logic_vector (  9 downto 0) := ( others => '0' );


	--==================================== Component Declaration ====================================--          


--=================================================================================================--
begin                 --========####   Architecture Body   ####========-- 
--=================================================================================================--

   --==================================== Port Mapping ==============================================--  

   --==================================== User Logic ==============================================--  
    RESET_O                               <= soft_reset or RESET_I or user_reset;
    CORECTL_O                             <= '1';
    
    --==================================== Avalon Bus Logic ==============================================--  
    avalon_read:
    process(RESET_I, csi_monitor_clk, avs_monitor_read)
    begin
        if RESET_I = '1' then
            avs_monitor_readdata <= (others=>'0');
        elsif rising_edge(csi_monitor_clk) then
                if avs_monitor_read = '1' and avs_monitor_address = "0" then
                    avs_monitor_readdata <= x"0000" & "000000" & temp_value;    
                else
                    avs_monitor_readdata <= (others=>'0');                    
                end if;
        end if;
    end process;

    avalon_write:
    process(RESET_I, csi_monitor_clk, avs_monitor_write)
    begin
        if RESET_I = '1' then
            user_reset <= '0';
        elsif rising_edge(csi_monitor_clk) then
                if avs_monitor_write = '1' and avs_monitor_address = "0" then
                    user_reset          <= avs_monitor_writedata(0);                
                end if;
        end if;
    end process;

    --================================= Reset Automation and Value Load ====================================---
    
    reset_automation_And_Value_load:
    process (RESET_I, csi_monitor_clk)
    begin
        if RESET_I = '1' then
            soft_reset                      <= '0';
            temp_value                      <= (others => '0');
        elsif rising_edge(csi_monitor_clk) then
            eoc_d31                         <= eoc_d31(30 downto 0) & EOC_I;
            if eoc_d31(4) = '1' and eoc_d31(3) = '0' then
                temp_value                  <= TEMPOUT_I;
            elsif eoc_d31(31) = '1' and eoc_d31(30) = '0' then
                soft_reset                  <= '1';
            else
                soft_reset                  <= '0';                
            end if;
            
        end if;
    end process;




end architecture mixed;