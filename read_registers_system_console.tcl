# Written By Jubin Mitra
# Dated: 17 - 02 - 2016

set master_service_path [lindex [get_service_paths master] 0]
open_service master $master_service_path

proc hex2dec {largeHex} {
    set res 0
    foreach hexDigit [split $largeHex {}] {
        set new 0x$hexDigit
        set res [expr {16*$res + $new}]
    }
    return $res
}
set temp_fpga_code [lindex [master_read_32 $master_service_path 0x48 0x2] 0]
set temp_fpga_hex [format "%0x" $temp_fpga_code]
set temp_fpga_num [hex2dec $temp_fpga_hex]
set temp_fpga_deg_celsius [expr (693*$temp_fpga_num/1024.00) - 265]
puts "FPGA Temperature (deg C) = $temp_fpga_deg_celsius"
puts "FPGA Temperature (deg F) = [expr (180*$temp_fpga_deg_celsius/100.00) + 32]"

set clk_si5338u26_code [lindex [master_read_32 $master_service_path [expr 0x20+(0x09<<2)] 0x2] 0]
set clk_si5338u26_hex [format "%0x" $clk_si5338u26_code]
set clk_si5338u26_num [hex2dec $clk_si5338u26_hex]



proc disp {value} {
master_write_32 $master_service_path [expr 0x00+(0x0<<2)] $value]
}